<?php

declare(strict_types=1);

require __DIR__ . '/../vendor/autoload.php';


$container = App\Bootstrap::boot()
	->createContainer();

if (!isset($argv[2])) {
	echo '
Add new user to database.

Usage: create-user.php <name> <password>
';
	exit(1);
}

[, $name, $password] = $argv;

$manager = $container->getByType(App\Model\UserFacade::class);


try {
    $manager->add($name, $password);
    echo "User $name was added.\n";

} catch (\App\Model\Exceptions\DuplicateNameException $e) {
    echo "Error: duplicate name.\n";
    exit(1);
}

