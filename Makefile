docker-exec-t=docker-compose exec -T app
docker-exec=docker-compose exec app
containerName = smislsoft-web_app_1

# Helpers & shortcuts
bash:
	${docker-exec} $(filter-out $@,$(MAKECMDGOALS))

composer:
	${docker-exec} composer $(filter-out $@,$(MAKECMDGOALS))

tobash:
	docker exec -it ${containerName} bash

phpstan:
	${docker-exec-t} vendor/bin/phpstan analyse -l 6 app --ansi

rm-cache:
	rm -rf temp/cache