let spring = 0.05;
let gravity = 0; //0.03;
let friction = -1; //-0.9;
let balls = [];
let running = true;
let colorsForBalls;
let clampSpeed = 1.5;
let BallsData = {
    0: {
        text: "Kámen\nnůžky\nvšechno",
        url: "http://knv.smislsoft.online"
    },
    1: {
        text: "Sebastianova\nknihovna",
        url: BASE_URI+"/fungus/knihovna/"
    },
    2: {
        text: "Plísně",
        url: BASE_URI+"/fungus/"
    },
    3: {
        text: "Jínění 2",
        url: BASE_URI+"/fungus/jineni2/"
    },
    4: {
        text: "Testament",
        url: BASE_URI+"/testament/"
    }
}

let letBallsDataKeys = Object.keys(BallsData);
let numBalls = letBallsDataKeys.length;


const clamp = (a, min = 0, max = 1) => Math.min(max, Math.max(min, a));

const invlerp = (x, y, a) => clamp((a - x) / (y - x));

function keyPressed() {
    if (keyCode === 32) {
        running = !running;
        for (let i = 0; i < numBalls; i++) {
            console.log("for i=" + i);
            console.log(balls[i].interactingColors);
        }
    }
}

const getComplementaryColor = (a) => {

    var r1 = 255 - a.levels[0];
    var g1 = 255 - a.levels[1];
    var b1 = 255 - a.levels[2];
    var comp = color(r1, g1, b1);
    return comp;

}

const getcomplementary = (a, b) => {

    var r1 = 255 - a.levels[0];
    var g1 = 255 - a.levels[1];
    var b1 = 255 - a.levels[2];
    var r2 = 255 - b.levels[0];
    var g2 = 255 - b.levels[1];
    var b2 = 255 - b.levels[2];
    var comp = lerpColor(color(r1, g1, b1), color(r2, g2, b2), 0.5);
    return comp;

}

function setup() {
    colorsForBalls = [
        color("#F7FF00"),
        color("#00ff87"),
        color("#0800FF"),
         color("#94D2BD"),
    color("#BB3E03"),
    ];
    let height = Math.min(window.innerHeight, 500);
    let width = Math.min(window.innerWidth, 1000);
    createCanvas(width, height);
    let minRadius = height/7;
    let maxRadius = height/5;
    for (let i = 0; i < numBalls; i++) {
        //fill(random(255),random(255), random(255))
        balls[i] = new Ball(
            random(width),
            random(height),
            random(minRadius, maxRadius),
            i,
            balls,
            BallsData[i]
        );
    }
    noStroke();
    for (let i = 0; i < numBalls; i++) {
        //fill(random(255),random(255), random(255))
        balls[i].AddOtherBalls(balls);
    }
    //fill(255, 204, 7, 100);
}



function draw() {
    if (running) {
        background("#001219");
        //console.log("one loop");
        balls.forEach(ball => {

            ball.collide();
            ball.coloring();
            ball.move();
            ball.display();
        });
    }
}

function IsColliding(x, y, radius) {
    // Test if the cursor is over the box
    return (mouseX > x - radius &&
        mouseX < x + radius &&
        mouseY > y - radius &&
        mouseY < y + radius);
}

function mouseClicked() {
    for (let i = 0; i < numBalls; i++) {
        if (IsColliding(balls[i].x, balls[i].y, balls[i].diameter / 2)) {
            window.location.href = BallsData[i].url;
            return;
        }
    }
}


class Ball {
    constructor(xin, yin, din, id, others, linkData) {
        this.linkData = linkData;
        this.log = false;
        this.x = xin;
        this.y = yin;
        this.vx = random(0.5,1.5);
        this.vy = random(0.5,1.5);
        this.diameter = din;
        this.id = id;
        this.others = others;
        this.baseColor = colorsForBalls[id % colorsForBalls.length]; //   color(random(255), random(255), random(255));
        //console.log(getcomplementary(this.baseColor, this.baseColor));
        this.color = this.baseColor;
        this.destinedColor = null; //this.color;
        this.interactingColors = {

        }

        for (let i = 0; i < numBalls; i++) {
            if (i == this.id) {
                this.interactingColors[i] = this.baseColor;
                //console.log("dasd" + this.interactingColors[i]._array[0]);
            } else {
                this.interactingColors[i] = null;
            }

        }

        this.CalculateDestinedColor();
    }

    AddOtherBalls(balls) {
        let otherBalls = [];
        for (let i = 0; i < balls.length; i++) {
            //fill(random(255),random(255), random(255))
            if (balls[i].id != this.id) {
                otherBalls.push(balls[i]);
            }
            this.otherBalls = otherBalls;
        }
    }

    CalculateDestinedColor() {
        let workingColor = null;
        let colorRGBASum = [0, 0, 0, 0];
        let colorCount = 0;
        //console.log("for id=" + this.id);
        for (let i = 0; i < numBalls; i++) {
            //console.log(this.interactingColors[i]);
            if (this.interactingColors[i] != null) {
                colorCount++;
                colorRGBASum[0] += this.interactingColors[i]._array[0];
                colorRGBASum[1] += this.interactingColors[i]._array[1];
                colorRGBASum[2] += this.interactingColors[i]._array[2];
                colorRGBASum[3] += this.interactingColors[i]._array[3];
            }
        }
        const colorRGBA = colorRGBASum.map(x => x / colorCount * 255);
        this.destinedColor = color(colorRGBA[0], colorRGBA[1], colorRGBA[2], colorRGBA[3]);

    }

    AddInteractingColor(color, ballIOndex) {
        this.interactingColors[ballIOndex] = color;
        this.CalculateDestinedColor();
    }

    RemoveInteractingColor(ballIOndex) {
        this.interactingColors[ballIOndex] = null;
        this.CalculateDestinedColor();
    }
    
    Clamp(value, max) {
        let result= value;
        if(value>max) {
            result = max;
        }
        return result;
    }

    collide() {
        if (this.log) {
            console.log("for id=" + this.id);
            console.log("loop on :");
        }
        for (let i = this.id + 1; i < numBalls; i++) {
            if (this.log) {
                console.log(i)
            }
            // console.log(others[i]);
            let dx = this.others[i].x - this.x;
            let dy = this.others[i].y - this.y;
            let distance = sqrt(dx * dx + dy * dy);
            let minDist = this.others[i].diameter / 2 + this.diameter / 2;
            let detectDistance = this.diameter + this.others[i].diameter;
            //  console.log(this.id + " minDist=" + minDist + " detectDistance=" + detectDistance);
            //console.log(minDist);
            if (distance < minDist) {
                //console.log("2");
                let angle = atan2(dy, dx);
                let targetX = this.x + cos(angle) * minDist;
                let targetY = this.y + sin(angle) * minDist;
                let ax = (targetX - this.others[i].x) * spring;
                let ay = (targetY - this.others[i].y) * spring;
                this.vx -= ax;
                this.vy -= ay;
                this.vx=this.Clamp(this.vx, clampSpeed);
                this.vy=this.Clamp(this.vy, clampSpeed);
                this.others[i].vx += ax;
                this.others[i].vy += ay;
                
                this.others[i].vx=this.Clamp(this.others[i].vx, clampSpeed);
                this.others[i].vy=this.Clamp(this.others[i].vy, clampSpeed);

            }
            if (distance < detectDistance) {
                if (this.interactingColors[i] == null) {

                    //console.log("adding color to " + this.id + " and " + i);
                    this.AddInteractingColor(this.others[i].baseColor, i);
                    this.others[i].AddInteractingColor(this.baseColor, this.id);
                }
            } else {
                if (this.interactingColors[i] != null) {
                    // console.log("removing color to " + this.id + " and " + i);
                    this.RemoveInteractingColor(i);
                    this.others[i].RemoveInteractingColor(this.id);
                }
            }

            //detectDistance = 0
            //minDist = 1
            /*let intensity = invlerp(detectDistance, minDist, distance);

            if (distance < detectDistance) {
                if (this.destinedColor == null) {
                    this.destinedColor = getcomplementary(this.baseColor, this.others[i].baseColor); // lerpColor(this.baseColor, this.others[i].baseColor, 0.5);
                    // console.log(this.connectedColor)
                    this.others[i].destinedColor = this.destinedColor;
                }
                /*
                                this.color = lerpColor(this.color, this.destinedColor, intensity);
                                this.others[i].color = lerpColor(this.others[i].color, this.others[i].connectedColor, intensity);
                */

            /*  } else {
                this.destinedColor = null;

            }
            // else {
            if (this.destinedColor == null) {
                this.color = lerpColor(this.color, this.baseColor, 0.05);
                this.others[i].color = lerpColor(this.others[i].color, this.others[i].baseColor, 0.05);
            } else {
                this.color = lerpColor(this.color, this.destinedColor, 0.05);
                this.others[i].color = lerpColor(this.others[i].color, this.others[i].destinedColor, 0.05);
            }
            // }
*/
        }
        this.color = lerpColor(this.color, this.destinedColor, 0.03);
        this.log = false;
    }

    coloring() {
        /*let detectDistance = this.diameter * 2;
        for (let i = 0; i < this.otherBalls.length; i++) {
            let dx = this.otherBalls[i].x - this.x;
            let dy = this.otherBalls[i].y - this.y;
            let distance = sqrt(dx * dx + dy * dy);

        }*/
    }

    move() {
        this.vy += gravity;
        this.x += this.vx;
        this.y += this.vy;
        if (this.x + this.diameter / 2 > width) {
            this.x = width - this.diameter / 2;
            this.vx *= friction;
        } else if (this.x - this.diameter / 2 < 0) {
            this.x = this.diameter / 2;
            this.vx *= friction;
        }
        if (this.y + this.diameter / 2 > height) {
            this.y = height - this.diameter / 2;
            this.vy *= friction;
        } else if (this.y - this.diameter / 2 < 0) {
            this.y = this.diameter / 2;
            this.vy *= friction;
        }
    }

    display() {
        // fill(random(255),random(255), random(255));
        fill(this.color);
        ellipse(this.x, this.y, this.diameter, this.diameter);
        /*fill(this.baseColor);
        textAlign(CENTER);
        text(   "id=" + this.id, this.x + 1, this.y + 1);*/
        fill(getComplementaryColor(this.color));
        textAlign(CENTER, CENTER);
        text(this.linkData.text, this.x, this.y); //
        //text("this is a link", this.x, this.y);

    }
}