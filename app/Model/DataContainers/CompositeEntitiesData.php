<?php declare(strict_types = 1);

namespace App\Model\DataContainers;

class CompositeEntitiesData
{

    /**
     * @param int $groupId
     * @param int[] $projectIds
     */
    public function __construct(
        public int $groupId,
        public array $projectIds,
    )
    {
    }

}
