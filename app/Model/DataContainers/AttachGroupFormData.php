<?php declare(strict_types = 1);

namespace App\Model\DataContainers;

class AttachGroupFormData
{

    /**
     * @param int $parentGroupId
     * @param int[] $childGroupIds
     */
    public function __construct(
        public int $parentGroupId,
        public array $childGroupIds,
    )
    {
    }

}
