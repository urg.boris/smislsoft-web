<?php declare(strict_types = 1);

namespace App\Model;

use App\Model\Exceptions\DuplicateNameException;
use App\Model\Orm\User\User;
use App\Model\Orm\User\UserRepository;
use Nette;
use Nette\Security\Passwords;
use Nextras\Dbal\Drivers\Exception\UniqueConstraintViolationException;

final class UserFacade implements Nette\Security\Authenticator
{

    use Nette\SmartObject;

    private UserRepository $repository;

    private Passwords $passwords;

    public function __construct(
        UserRepository $repository,
        Passwords $passwords,
    )
    {
        $this->repository = $repository;
        $this->passwords = $passwords;
    }


    /**
     * Performs an authentication.
     *
     * @throws Nette\Security\AuthenticationException
     */
    public function authenticate(string $username, string $password): Nette\Security\SimpleIdentity
    {
        /** @var User|null $user */
        $user = $this->repository->getBy(['name' => $username]);

        if ($user === null) {
            throw new Nette\Security\AuthenticationException('The username is incorrect.', self::IDENTITY_NOT_FOUND);
        }

        if (!$this->passwords->verify($password, $user->pass)) {
            throw new Nette\Security\AuthenticationException('The password is incorrect.', self::INVALID_CREDENTIAL);

        } elseif ($this->passwords->needsRehash($user->pass)) {
            $user->pass = $this->passwords->hash($password);
            $this->repository->persistAndFlush($user);
        }

        $arr = $user->toArray();
        unset($arr['pass']);
        return new Nette\Security\SimpleIdentity($user->id, $user->role, $arr);
    }


    /**
     * Adds new user.
     *
     * @throws DuplicateNameException
     */
    public function add(string $username, string $password): void
    {
        try {
            $newUser = new User();
            $newUser->name = $username;
            $newUser->pass = $this->passwords->hash($password);
            $newUser->role = User::TYPE_ADMIN;

            $this->repository->persistAndFlush($newUser);
        } catch (UniqueConstraintViolationException $e) {
            throw new DuplicateNameException();
        }
    }

}
