<?php declare(strict_types = 1);

namespace App\Model\Orm\User;

use App\Model\Orm\CommonEntity;
use App\Model\Orm\CommonRepository;
use App\Model\Orm\Group\Group;

class UserRepository extends CommonRepository
{

	public static function getEntityClassNames(): array
	{
		return [User::class];
	}

    public function createNewEntity(): CommonEntity
    {
        return new Group();
    }

}
