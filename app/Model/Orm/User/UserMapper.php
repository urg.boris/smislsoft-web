<?php declare(strict_types = 1);

namespace App\Model\Orm\User;

use App\Model\Orm\CommonMapper;

class UserMapper extends CommonMapper
{

	public function getTableName(): string
	{
		return 'user';
	}

}
