<?php declare(strict_types = 1);

namespace App\Model\Orm\User;

use App\Model\Orm\CommonEntity;

/**
 * @property int $id {primary}
 * @property string $name
 * @property string $pass
 * @property string $role {enum self::TYPE_*}
 */
class User extends CommonEntity
{

    public const TYPE_ADMIN  = 'admin';
    public const TYPE_STALKER = 'stalker';
    public const TYPE_GUEST = 'guest';

}
