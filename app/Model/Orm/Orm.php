<?php declare(strict_types = 1);

namespace App\Model\Orm;

use App\Model\Orm\Group\GroupRepository;
use App\Model\Orm\GroupGroup\GroupGroupRepository;
use App\Model\Orm\GroupProject\GroupProjectRepository;
use App\Model\Orm\Project\ProjectRepository;
use App\Model\Orm\User\UserRepository;
use Nextras\Orm\Model\Model;

/**
 * @property-read GroupRepository $group
 * @property-read ProjectRepository $project
 * @property-read GroupGroupRepository $groupGroup;
 * @property-read GroupProjectRepository $groupProject;
 * @property-read UserRepository $user
 */
class Orm extends Model
{

}
