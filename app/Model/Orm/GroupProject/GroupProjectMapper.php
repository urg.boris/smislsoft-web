<?php declare(strict_types = 1);

namespace App\Model\Orm\GroupProject;

use App\Model\Orm\CommonMapper;

class GroupProjectMapper extends CommonMapper
{

	public function getTableName(): string
	{
		return 'group_x_project';
	}

}
