<?php declare(strict_types = 1);

namespace App\Model\Orm\GroupProject;

use App\Model\Orm\CommonEntity;
use App\Model\Orm\CommonRepository;

class GroupProjectRepository extends CommonRepository
{

	public static function getEntityClassNames(): array
	{
		return [GroupProject::class];
	}

    public function createNewEntity(): CommonEntity
    {
        return new GroupProject();
    }

}
