<?php declare(strict_types = 1);

namespace App\Model\Orm\GroupProject;

use App\Model\Orm\CommonEntity;
use App\Model\Orm\Group\Group;
use App\Model\Orm\Project\Project;

/**
 * @property int $id {primary}
 * @property Group $group {1:1 Group, isMain=true, oneSided=true};
 * @property Project $project {1:1 Project, isMain=true, oneSided=true};
 */
class GroupProject extends CommonEntity
{

}
