<?php declare(strict_types = 1);

namespace App\Model\Orm\Composite;

use App\Model\Orm\CommonFacade;

class CompositeEntityFacade extends CommonFacade
{

    /**
     * @return CompositeEntity[]
     */
    public function getCompositeEntities(string $groupName): array
    {
        return [];
    }
    //TODO

}
