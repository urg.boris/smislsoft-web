<?php declare(strict_types = 1);

namespace App\Model\Orm\Composite;

use App\Model\Orm\CommonEntity;

/**
 * @property int $id {primary}
 * @property string $name
 * @property string $contentName
 * @property bool $isGroup {virtual}
 */
abstract class CompositeEntity extends CommonEntity
{

    abstract public function getterIsGroup(): bool;

}
