<?php declare(strict_types = 1);

namespace App\Model\Orm\Project;

use App\Model\Orm\CommonMapper;

class ProjectMapper extends CommonMapper
{

	public function getTableName(): string
	{
		return 'project';
	}

}
