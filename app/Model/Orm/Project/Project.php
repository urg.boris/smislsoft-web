<?php declare(strict_types = 1);

namespace App\Model\Orm\Project;

use App\Model\Orm\Composite\CompositeEntity;
use App\Model\Orm\Group\Group;
use Nextras\Orm\Relationships\ManyHasMany;

/**
 * @property string $tech
 * @property string $time
 * @property int $imageCount {default 0}
 * @property string|null $imagePath
 * @property string|NULL $contentHtml
 * @property string|NULL $git
 * @property string|NULL $playLink
 * @property string|NULL $downloadPath;
 * @property string|NULL $benefit
 * @property string|NULL $pain
 * @property ManyHasMany<Group> $groups {m:m Group::$projects}
 */
class Project extends CompositeEntity
{

    public function getterIsGroup(): bool
    {
        return false;
    }

}
