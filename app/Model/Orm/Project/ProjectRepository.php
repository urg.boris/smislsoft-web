<?php declare(strict_types = 1);

namespace App\Model\Orm\Project;

use App\Model\Orm\CommonEntity;
use App\Model\Orm\CommonRepository;

class ProjectRepository extends CommonRepository
{

	public static function getEntityClassNames(): array
	{
		return [Project::class];
	}
    public function createNewEntity(): CommonEntity
    {
        return new Project();
    }

}
