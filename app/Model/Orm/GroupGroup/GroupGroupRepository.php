<?php declare(strict_types = 1);

namespace App\Model\Orm\GroupGroup;

use App\Model\Orm\CommonEntity;
use App\Model\Orm\CommonRepository;

class GroupGroupRepository extends CommonRepository
{

	public static function getEntityClassNames(): array
	{
		return [GroupGroup::class];
	}
    public function createNewEntity(): CommonEntity
    {
        return new GroupGroup();
    }

}
