<?php declare(strict_types = 1);

namespace App\Model\Orm\GroupGroup;

use App\Model\Orm\CommonEntity;
use App\Model\Orm\Group\Group;

/**
 * @property int $id {primary}
 * @property Group $parentGroup {m:1 Group::$subgroups}
 * @property Group $childGroup {1:1 Group, isMain=true, oneSided=true}
 */
class GroupGroup extends CommonEntity
{

}
