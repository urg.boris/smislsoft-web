<?php declare(strict_types = 1);

namespace App\Model\Orm\GroupGroup;

use App\Model\Orm\CommonMapper;

class GroupGroupMapper extends CommonMapper
{

	public function getTableName(): string
	{
		return 'group_x_group';
	}

}
