<?php declare(strict_types = 1);

namespace App\Model\Orm\Group;

use App\Model\Orm\Composite\CompositeEntity;
use App\Model\Orm\GroupGroup\GroupGroup;
use App\Model\Orm\Project\Project;
use Nextras\Orm\Relationships\ManyHasMany;
use Nextras\Orm\Relationships\OneHasMany;

/**
 * @property string|null $contentHtml
 * @property int $time
 * @property ManyHasMany<Project> $projects {m:m Project::$groups, isMain=true, orderBy=[time=DESC]}
 * @property OneHasMany<GroupGroup> $subgroups {1:m GroupGroup::$parentGroup}
 * @property ManyHasMany<Group> $childGroups {m:m Group::$parentGroups, orderBy=[time=DESC]}
 * @property ManyHasMany<Group> $parentGroups {m:m Group::$childGroups, isMain=true}
 */

class Group extends CompositeEntity
{

    public function getterIsGroup(): bool
    {
        return true;
    }

}
