<?php declare(strict_types = 1);

namespace App\Model\Orm\Group;

use App\Model\Orm\CommonEntity;
use App\Model\Orm\CommonRepository;

class GroupRepository extends CommonRepository
{

    public const DEFAULT_GROUP_NAME = 'main';

	public static function getEntityClassNames(): array
	{
		return [Group::class];
	}
    public function createNewEntity(): CommonEntity
    {
        return new Group();
    }

}
