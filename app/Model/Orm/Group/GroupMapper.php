<?php declare(strict_types = 1);

namespace App\Model\Orm\Group;

use App\Model\Orm\CommonMapper;

class GroupMapper extends CommonMapper
{

	public function getTableName(): string
	{
		return 'group';
	}

}
