<?php declare(strict_types = 1);

namespace App\Presenters;

use App\Model\Orm\Composite\CompositeEntity;
use App\Model\Orm\Group\Group;
use App\Model\Orm\Group\GroupRepository;
use App\Model\Orm\Project\ProjectRepository;
use App\Router\RouterFactory;

class FungusListPresenter extends BaseTemplatePresenter
{

    /** @var GroupRepository @inject */
    public GroupRepository $groupRepository;

    /** @var ProjectRepository @inject */
    public ProjectRepository $projectRepository;

    public function renderDefault(): void
    {
        parent::beforeRender();
        $groupName = $this->getParameter(RouterFactory::PROJECT_GROUP_NAME);
        if ($groupName === null) {
            $groupName = GroupRepository::DEFAULT_GROUP_NAME;
        }

        /** @var Group|null $groupEntity */
        $groupEntity = $this->groupRepository->getBy(['name' => $groupName]);

        if ($groupEntity === null) {
            $this->error('no project group ' . $groupName);
        }

        $this->template->group = $groupEntity;
        $this->template->maxLevel = 1;

        //TODO to facade
        $projects = $this->projectRepository->findBy(['groups->name' => $groupName]);

        /** @var CompositeEntity[] $compositeEntities */
        $compositeEntities = [];

        $childGroups = $groupEntity->childGroups->toCollection();

        foreach ($childGroups as $childGroup) {
            $compositeEntities[] = $childGroup;
        }

        foreach ($projects as $project) {
            $compositeEntities[] = $project;
        }

        $this->template->compositeEntities = $compositeEntities;
    }

}
