<?php declare(strict_types = 1);

namespace App\Presenters;

use App\Model\Orm\Composite\CompositeEntity;
use App\Model\Orm\Group\Group;
use App\Model\Orm\Group\GroupRepository;
use App\Model\Orm\Project\ProjectRepository;
use App\Router\RouterFactory;

final class ProjectGroupPresenter extends BaseTemplatePresenter
{

    /** @var GroupRepository @inject */
    public GroupRepository $groupRepository;

    /** @var ProjectRepository @inject */
    public ProjectRepository $projectRepository;

    /**
     * @return string[]
     */
    public function formatTemplateFiles(): array
    {
        $templateDir = 'Project';
        $dir = dirname((string) $this->getReflection()->getFileName());
        return [
            sprintf('%s/templates/%s/%s.latte', $dir, $templateDir, $this->view),
        ];
    }

    protected function beforeRender(): void
    {
        parent::beforeRender();
        $groupName = $this->getParameter(RouterFactory::PROJECT_GROUP_NAME);
        if ($groupName === null) {
            $groupName = GroupRepository::DEFAULT_GROUP_NAME;
        }

        /** @var Group|null $groupEntity */
        $groupEntity = $this->groupRepository->getBy(['name' => $groupName]);

        if ($groupEntity === null) {
            $this->error('no project group ' . $groupName);
        }

        $this->template->group = $groupEntity;

        $projects = $this->projectRepository->findBy(['groups->name' => $groupName]);

        /** @var CompositeEntity[] $compositeEntities */
        $compositeEntities = [];

        $childGroups = $groupEntity->childGroups->toCollection();
        foreach ($childGroups as $childGroup) {
            $compositeEntities[] = $childGroup;
        }

        foreach ($projects as $project) {
            $compositeEntities[] = $project;
        }

        $this->template->compositeEntities = $compositeEntities;

        $projectName = $this->getParameter(RouterFactory::PROJECT_NAME);
        if ($projectName !== null) {
            $projectEntity = $this->projectRepository->getBy(['name' => $projectName]);

            if ($projectEntity === null) {
                $this->error('no project ' . $projectName);
            }

            $this->template->entity = $projectEntity;
        }
    }

}
