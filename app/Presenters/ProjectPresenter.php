<?php declare(strict_types = 1);

namespace App\Presenters;

use App\Model\Orm\Group\GroupRepository;
use App\Model\Orm\Project\ProjectRepository;
use App\Router\RouterFactory;

final class ProjectPresenter extends BaseTemplatePresenter
{

    /** @var ProjectRepository @inject */
    public ProjectRepository $projectRepository;

    /** @var GroupRepository @inject */
    public GroupRepository $groupRepository;

    protected function beforeRender(): void
    {
        parent::beforeRender();
        $projectName = $this->getParameter(RouterFactory::PROJECT_NAME);

        $entity = $this->projectRepository->getBy(['name' => $projectName]);
        if ($entity === null) {
            $this->error('no project ' . $projectName);
        }

        $this->template->entity = $entity;
    }

}
