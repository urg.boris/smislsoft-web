<?php declare(strict_types = 1);

namespace App\Presenters;

use Nette;
use Tracy\Debugger;

abstract class BaseTemplatePresenter extends Nette\Application\UI\Presenter
{

    protected function beforeRender(): void
    {
        parent::beforeRender();
        $path = __DIR__ . '/templates/Common/@layout.latte';
        $this->setLayout($path);
        $this->template->isDev = !Debugger::$productionMode;
       /* \Tracy\Debugger::dump( $this->template->isDev);
        die;*/
    }

}
