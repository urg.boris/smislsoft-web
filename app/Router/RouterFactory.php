<?php declare(strict_types = 1);

namespace App\Router;

use Nette;
use Nette\Application\Routers\RouteList;

final class RouterFactory
{

    use Nette\StaticClass;

    public const PROJECT_NAME = 'projectName';
    public const PROJECT_GROUP_NAME = 'projectGroupName';

    public static function createRouter(): RouteList
    {
        $router = new RouteList();
        $router->addRoute('fungus[/]', 'FungusList:default');
        $router->addRoute(sprintf('fungus/[<%s>][/<%s>]', self::PROJECT_GROUP_NAME, self::PROJECT_NAME), 'ProjectGroup:default');
        $router->addRoute(sprintf('project/<%s>', self::PROJECT_NAME), 'Project:default');
        $router->addRoute('<presenter>[/<action>][/<id>]', 'Homepage:default');
        $router->addRoute('[<module>/]<presenter>[/<action>][/<id>]', 'Homepage:default');


        return $router;
    }

}
