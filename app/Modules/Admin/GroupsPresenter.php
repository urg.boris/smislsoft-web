<?php declare(strict_types = 1);

namespace App\Modules\Admin;

use App\Model\DataContainers\AttachGroupFormData;
use App\Model\DataContainers\AttachProjectFormData;
use App\Model\Orm\Group\Group;
use App\Model\Orm\Group\GroupRepository;
use App\Model\Orm\GroupGroup\GroupGroup;
use App\Model\Orm\GroupGroup\GroupGroupRepository;
use App\Model\Orm\GroupProject\GroupProject;
use App\Model\Orm\GroupProject\GroupProjectRepository;
use App\Model\Orm\Project\Project;
use App\Model\Orm\Project\ProjectRepository;
use Nette\Application\Helpers;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;

class GroupsPresenter extends EntityEditorPresenter
{

    /** @var ProjectRepository @inject */
    public ProjectRepository $projectRepository;

    /** @var GroupGroupRepository @inject */
    public GroupGroupRepository $groupGroupRepository;

    /** @var GroupProjectRepository @inject */
    public GroupProjectRepository $groupProjectRepository;

    public function __construct(GroupRepository $entityRepository)
    {
        parent::__construct($entityRepository);
    }

    public function createComponentAttachProjectForm(): Form
    {
        $projectList = $this->projectRepository->findAll()->fetchPairs('id', 'name');
        $form = new Form();
        $form->addMultiSelect('projectIds', 'Projects', $projectList);
        $form->addHidden('groupId', $this->getParameter('id'))->addFilter(function ($item) {
            return (int) $item;
        });

        $form->addSubmit('send', 'Attach');

        $form->onSuccess[] = function (Form $form, ArrayHash $values): void {
            /** @var AttachProjectFormData $data */
            $data = $form->getValues(AttachProjectFormData::class);

            if (count($data->projectIds) > 0) {
                $groupId = $data->groupId;

                /** @var Group $groupEntity */
                $groupEntity = $this->entityRepository->getById($groupId);
                foreach ($data->projectIds as $projectId) {
                    $entityRelation = $this->projectRepository->getBy(['id' => $projectId, 'groups->id' => $groupId]);
                    if ($entityRelation !== null) {
                        $this->error(sprintf('entity projectid=%d already exists in group=%d', $projectId, $groupId));
                    }

                    /** @var Project $projectEntity */
                    $projectEntity = $this->projectRepository->getById($projectId);
                    $entity = new GroupProject();
                    $entity->group = $groupEntity;
                    $entity->project = $projectEntity;
                    $this->orm->persist($entity);
                }

                $this->orm->flush();
            }

            $this->redirect(':edit', $data->groupId);
        };

        return $form;
    }

    public function createComponentAttachGroupForm(): Form
    {
        $entityList = $this->entityRepository->findAll()->fetchPairs('id', 'name');
        $form = new Form();
        $form->addMultiSelect('childGroupIds', 'Groups', $entityList);
        $form->addHidden('parentGroupId', $this->getParameter('id'))->addFilter(function ($item) {
            return (int) $item;
        });

        $form->addSubmit('send', 'Attach');

        $form->onSuccess[] = function (Form $form, ArrayHash $values): void {
            /** @var AttachGroupFormData $data */
            $data = $form->getValues(AttachGroupFormData::class);

            if (count($data->childGroupIds) > 0) {
                $parentGroupId = $data->parentGroupId;
                /** @var Group $parentGroupEntity */
                $parentGroupEntity = $this->entityRepository->getById($parentGroupId);

                foreach ($data->childGroupIds as $childGroupId) {
                    $entityRelation = $this->entityRepository->getBy([
                        'parentGroups->id' => $parentGroupId,
                        'childGroups->id' => $childGroupId,
                    ]);
                    if ($entityRelation !== null) {
                        $this->error(sprintf(
                            'entity groupid=%s already exists in group=%s',
                            $childGroupId,
                            $parentGroupId
                        ));
                    }

                    if ($parentGroupId === $childGroupId) {
                        $this->error(sprintf(
                            'entity childGroupId=%s is same as parentGroupId=%s',
                            $childGroupId,
                            $parentGroupId
                        ));
                    }

                    /** @var Group $childGroupEntity */
                    $childGroupEntity = $this->entityRepository->getById($childGroupId);
                    $entity = new GroupGroup();
                    $entity->parentGroup = $parentGroupEntity;
                    $entity->childGroup = $childGroupEntity;
                    $this->orm->persist($entity);
                }

                $this->orm->flush();
            }

            $this->redirect(':edit', $data->parentGroupId);
        };

        return $form;
    }

    /**
     * @return string[]
     */
    public function formatTemplateFiles(): array
    {
        /** @var string $presenterName */
        $presenterName = $this->getName();
        [, $presenter] = Helpers::splitName($presenterName);
        $dir = dirname((string) $this->getReflection()->getFileName());
        return [
            sprintf('%s/templates/%s/%s.latte', $dir, $presenter, $this->view),
        ];
    }

    public function renderEdit(int $id): void
    {
        parent::renderEdit($id);

        $this->template->projects = $this->projectRepository->findBy(['groups->id' => $id]);
        $this->template->subgroups = $this->entityRepository->findBy(['parentGroups->id' => $id]);

        $this->template->groupId = $id;
    }

    public function actionDettachGroup(): void
    {
        $parentGroupId = $this->getParameter('parentGroupId');
        $childGroupId = $this->getParameter('childGroupId');

        /** @var GroupGroup $entity */
        $entity = $this->groupGroupRepository->getBy([
            'parentGroup->id' => $parentGroupId,
            'childGroup->id' => $childGroupId,
        ]);

        $this->orm->removeAndFlush($entity);

        $this->redirect(':edit', $parentGroupId);
    }



    public function actionDettachProject(): void
    {
        $groupId = $this->getParameter('groupId');
        $projectId = $this->getParameter('projectId');

        /** @var GroupProject $entity */
        $entity = $this->groupProjectRepository->getBy(['group->id' => $groupId, 'project->id' => $projectId]);

        $this->orm->removeAndFlush($entity);

        $this->redirect(':edit', $groupId);
    }

}
