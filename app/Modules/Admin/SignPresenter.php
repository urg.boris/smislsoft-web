<?php declare(strict_types = 1);

namespace App\Modules\Admin;

use App\Model\Orm\User\UserRepository;
use Nette;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;

final class SignPresenter extends Nette\Application\UI\Presenter
{

    /** @persistent */
    public string $backlink = '';

    /** @var UserRepository $repository @inject */
    public UserRepository $repository;

    protected function createComponentSignInForm(): Form
    {
        $form = new Form();
        $form->addText('name', 'name');
        $form->addPassword('pass', 'pass');
        $form->addSubmit('send', 'login');

        $form->onSuccess[] = function (Form $form, ArrayHash $values): void {
            try {
                $this->user->setExpiration('20 minutes');
                $this->user->login($values->name, $values->pass);
            } catch (Nette\Security\AuthenticationException $e) {
                $form->addError('The username or password you entered is incorrect.');
                return;
            }

            $this->restoreRequest($this->backlink);
            $this->redirect('Groups:default');
        };

        return $form;
    }





    public function actionOut(): void
    {
        $this->getUser()->logout();
        $this->redirect(':in');
    }

}
