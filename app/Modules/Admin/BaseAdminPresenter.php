<?php declare(strict_types = 1);

namespace App\Modules\Admin;

use Nette\Application\UI\Presenter;

abstract class BaseAdminPresenter extends Presenter
{

    use UserLogger;

}
