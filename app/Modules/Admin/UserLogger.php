<?php declare(strict_types = 1);

namespace App\Modules\Admin;

trait UserLogger
{

    public function injectUserLogger(): void
    {
        $this->onStartup[] = function () {
            if (!$this->getUser()->isLoggedIn()) {
                $this->redirect('Sign:in', ['backlink' => $this->storeRequest()]);
            }
        };
    }

}
