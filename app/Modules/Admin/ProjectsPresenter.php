<?php declare(strict_types = 1);

namespace App\Modules\Admin;

use App\Model\Orm\Project\ProjectRepository;

class ProjectsPresenter extends EntityEditorPresenter
{

    public function __construct(ProjectRepository $entityRepository)
    {
        parent::__construct($entityRepository);
    }

}
