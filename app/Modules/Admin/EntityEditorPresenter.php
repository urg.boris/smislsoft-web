<?php declare(strict_types = 1);

namespace App\Modules\Admin;

use App\Model\Orm\CommonRepository;
use App\Model\Orm\Group\Group;
use App\Model\Orm\Orm;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;

abstract class EntityEditorPresenter extends BaseAdminPresenter
{

    public const CONTENT_HTML = 'contentHtml';

    /** @var Orm @inject */
    public $orm;

    /** @var CommonRepository */
    protected CommonRepository $entityRepository;

    public function __construct(CommonRepository $entityRepository)
    {
        $this->entityRepository = $entityRepository;
    }

    /**
     * @return string[]
     */
    public function formatTemplateFiles(): array
    {
        $templateDir = 'EntityEditor';
        $dir = dirname((string) $this->getReflection()->getFileName());
        return [
            sprintf('%s/templates/%s/%s.latte', $dir, $templateDir, $this->view),
        ];
    }

    public function renderDefault(): void
    {
        $entities = $this->entityRepository->findAll();

        if ($entities->count() === 0) {
            $this->error('not found');
        }

        $this->template->entities = $entities;
    }

    protected function createComponentEntityForm(): Form
    {
        $form = new Form();
        $entityProperties = $this->entityRepository->getEntityMetadata()->getProperties();
        //TODO collections
        foreach ($entityProperties as $property) {
            if ($property->isPrimary || $property->isVirtual) {
                continue;
            }

            if ($property->name === self::CONTENT_HTML) {
                $formField = $form->addTextArea($property->name, $property->name);
            } else {
                $formField = $form->addText($property->name, $property->name);
                if (!$property->isNullable) {
                    $formField->setRequired();
                }
            }
        }

        $form->addSubmit('send', 'Save');

        $form->onSuccess[] = function (Form $form, ArrayHash $values): void {
            $this->saveToDb($values);
        };

        return $form;
    }

    protected function saveToDb(ArrayHash $values): void
    {
        $entityId = $this->getParameter('id');

        if ($entityId) {
            $entity = $this->entityRepository->getById($entityId);
        } else {
            $entity = $this->entityRepository->createNewEntity();
        }

        if ($entity === null) {
            $this->error('cant create entity');
        }

        foreach ($values as $name => $value) {
            $entity->setValue($name, $value);
        }

        $this->orm->persistAndFlush($entity);

        $this->redirect(':default');
    }

    public function renderCreate(): void
    {
    }

    public function renderEdit(int $id): void
    {
        /**
         * @var Group|null $entity
         */
        $entity = $this->entityRepository->getById($id);

        if ($entity === null) {
            $this->error('Entity id=' . $id . ' not found');
        }

        $this->getComponent('entityForm')
            ->setDefaults($entity->toArray());
    }

}
