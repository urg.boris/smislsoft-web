CREATE DATABASE IF NOT EXISTS `smislsoft_web` CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

GRANT ALL ON `smislsoft_web`.* TO 'root'@'%';

FLUSH PRIVILEGES;