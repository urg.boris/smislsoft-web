-- Adminer 4.8.1 MySQL 5.7.36 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `group`;
CREATE TABLE `group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8mb4_czech_ci NOT NULL,
  `content_name` varchar(250) COLLATE utf8mb4_czech_ci NOT NULL,
  `content_html` text COLLATE utf8mb4_czech_ci,
  `time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci;

INSERT INTO `group` (`id`, `name`, `content_name`, `content_html`, `time`) VALUES
(1,	'jineni',	'Jínění',	'',	0),
(2,	'knihovna',	'Knihovna',	'',	0),
(3,	'jineni2',	'Jínění 2  - prototypy',	'',	0),
(4,	'game',	'GameDev',	'',	0),
(5,	'main',	'Main',	'',	0),
(6,	'web',	'WebDev',	'',	0);

DROP TABLE IF EXISTS `group_x_group`;
CREATE TABLE `group_x_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_group_id` int(11) NOT NULL,
  `child_group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `parent_group_id_child_group_id` (`parent_group_id`,`child_group_id`),
  KEY `child_group_id` (`child_group_id`),
  CONSTRAINT `group_x_group_ibfk_2` FOREIGN KEY (`child_group_id`) REFERENCES `group` (`id`) ON DELETE CASCADE,
  CONSTRAINT `group_x_group_ibfk_4` FOREIGN KEY (`parent_group_id`) REFERENCES `group` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `group_x_group` (`id`, `parent_group_id`, `child_group_id`) VALUES
(12,	1,	3),
(15,	2,	3),
(14,	4,	3),
(18,	5,	4),
(16,	5,	6);

DROP TABLE IF EXISTS `group_x_project`;
CREATE TABLE `group_x_project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id_project_id` (`group_id`,`project_id`),
  KEY `project_id` (`project_id`),
  CONSTRAINT `group_x_project_ibfk_4` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `group_x_project_ibfk_5` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `group_x_project` (`id`, `group_id`, `project_id`) VALUES
(23,	1,	9),
(24,	1,	10),
(17,	2,	15),
(18,	2,	16),
(19,	3,	11),
(20,	3,	12),
(21,	3,	13),
(22,	3,	14),
(12,	4,	4),
(13,	4,	5),
(14,	4,	6),
(15,	4,	7),
(16,	4,	8),
(25,	4,	15),
(26,	4,	16),
(27,	6,	5),
(28,	6,	17),
(29,	6,	18),
(30,	6,	19);

DROP TABLE IF EXISTS `project`;
CREATE TABLE `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `content_name` varchar(250) NOT NULL,
  `tech` varchar(250) NOT NULL,
  `time` varchar(250) NOT NULL,
  `image_path` varchar(250) DEFAULT NULL,
  `image_count` int(11) NOT NULL,
  `content_html` text,
  `git` varchar(250) DEFAULT NULL,
  `play_link` varchar(250) DEFAULT NULL,
  `download_path` varchar(250) DEFAULT NULL,
  `benefit` varchar(250) DEFAULT NULL,
  `pain` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `project` (`id`, `name`, `content_name`, `tech`, `time`, `image_path`, `image_count`, `content_html`, `git`, `play_link`, `download_path`, `benefit`, `pain`) VALUES
(4,	'jadgarrul',	'JadGarrul',	'Unity, Arduino, BlueBooth, Android, PDF/JPG/PNG, Chemoprén',	'2021',	'data-plisne/jadgarrul%d.png',	4,	'<p>\r\n            Typický problém hráčů na hudební nástroje: pokud hraju rukami jak mohu otáčet stránky notového zápisu? To přece není tak náročné vytvořit, že?\r\n        </p>',	'https://gitlab.com/urg.boris/jadgarrul/',	'',	NULL,	'Arduino pokusy, dráty s kombinaci s dřevem',	'Bluetooth je vratký'),
(5,	'KNV',	'Kámen nůžky všechno',	'HTML5, Nette, React, Neo4j/MySQL, 3rd party API(generování medialních obsahů)',	'2022',	'',	0,	'<p>\n        Hrál si někdy kámen nůžky papír a nudila tě předvídatelnost? Málo útočných možností? Strohý visuál? Babička ti dala na talíř moc kledlíků? \n        </p>',	'https://gitlab.com/urg.boris/kamen-nuzky-vsechno/',	'http://knv.smislsoft.online/',	NULL,	'NPM(+react) web devel, neo4j+cypher',	'neo4j zdarma je nepoužitelný'),
(6,	'selfie',	'Selfie s Míou',	'GIMP, Unity+Rider, Vuforia, Android',	'2022',	'data-plisne/selfie%d.jpg',	3,	'<p>\r\n            Co dělat když <a href=\"http://miapedia.cz/\">kočka Mía</a> slaví svoje narozeniny a lidé si s ní chtějí udělat památeční selfie?<br/>            \r\n            Pokud by se jednalo o běžnou přítulnou kočičku, šlo by to hladce, bohužel <a href=\"http://miapedia.cz/\">Mía</a> je částěčně šlechtické krve a její postoje k lidem a jejich zvykům bývají mnohdy extrémní.<br/>\r\n            Navíc co když spousta jejích kamarádů nemůže na oslavu z časových důvodů dorazit? <br/>\r\n            \r\n        </p>\r\n        <p>\r\n            Aplikace pomůže běžným lidským srmtelníkům získat cenné selfie s <a href=\"http://miapedia.cz/\">Míou</a> a radovat se z něj ve svých jinak prázdných životech. \r\n        </p>',	'https://gitlab.com/urg.boris/selfie-s-miou/',	'',	'selfie.apk',	'Editace obrázků v GIMPu, ARCore,  faking 3d, 3.5d, 4d in vuforia',	'android building again...'),
(7,	'basefall',	'Basefall',	'Unreal, C++, Blueprint',	'2022',	'data-plisne/basefall-%d.png',	0,	'<p>\n            Bolely mě zuby, tahle simulace mi to připomíná. Taky mi připomíná životní snahy a jejich odraz ve vesmíru.\n        </p>\n        <p>\n            První experimenty s Unreal enginem a jeho blueprinty. Na C++ taky přišlo. Něco překvapivě dobrý, něco nepochopitelně skoro nemožný.\n        </p>',	'https://gitlab.com/urg.boris/basefall/',	'',	'basefall.rar',	'orientace v unrýlu, osvěžení C++, jo nody jsou teď všude',	'kde je to správný zaškrtávátko? frustrace ze snahy udělat něco jednoduchého, a zjišťovat proč se to tam dělá jinak'),
(8,	'proceduralno',	'Procedurálnosť',	'Houdini, Blender, Python+Visual Studio Code',	'2022',	'data-plisne/proceduralno-%d.png',	0,	'<p>\n            Prozkoumávání vesmíru 3D grafiky v Houdini Enginu a Blenderu. Renderování mě sice porád nezajímá, stačí mi View okno, ale tvořit geometrie je chutné. Návíc se tam daj psát python scripty pro zautomatizování práce, to je svět ve kterém chci žít.\n        </p>',	'',	'',	'blender+houdiny.rar',	'Totální uchvácení abstrakními možnostmi a nemožnostmi procedurální tvorby',	'Tolik možností jak tvořit, až to opravdu bolí. Škoda, že nejlepší tutoriály jsou v japonštině'),
(9,	'jineni0',	'Jínění 0',	'Git, Adobe flash',	'2012',	'data-minas/minas-%d.jpg',	3,	'<p>\n        Prázdný prostor po dokončení <a href=\"/project/minas/\">Minas Canabis</a> je vyplňován nekonečným osvěžením Adobe Flashe a nezadržitelným pnutím stvořit nadčasový kolos shrnující vesmír a ty další věcy.\n        </p>',	'https://gitlab.com/urg.boris/jineni-oldflash/',	'',	'jineni0.rar',	'Tvorba, recyklace, rozlepovaní slepých uliček scénáře nás již nedokáže zabrzdit',	've flashi to hrozně trvá. Navíc jsme zjistili, že pokud něco vymyslíme tak to pak nějak musíme vytvořit a to nám nikdo neřek'),
(10,	'jineni1',	'Jínění 1',	'Unity custom editor tools',	'2017',	'data-jineni/jineni1-%d.jpg',	4,	'<p>\r\n        Vybaveni dostatečným optimismen, přecházíme na unity a doufáme, že změna herního enginu nám přinese odpovědi na základní otázku.\r\n         </p>',	'https://gitlab.com/urg.boris/jineni',	'',	'jineni1embryo.rar',	'Unity custom node editor pro tvorbu dialog driven adventury',	'Programovat sice umíme lépjeji, ale domlouvat se neumíme'),
(11,	'uvodnik',	'Úvodník',	'Unity, Fmod, mbti',	'2021',	'data-jineni/jineni2-uvodnik-%d.jpg',	1,	'<p>\n             Pokusy se začleněním MBTI modelu do plnosti narativu a jeho použití u rozhodovacích stromů.\n         </p>',	'https://gitlab.com/urg.boris/jineni2-uvodnik',	'',	'uvodnik.rar',	'FMOD v Unity, teorie MBTI modelu/kognitivních funkcí',	'Unity UI editor/playmode, použitelnost MBTI pro projekt stále nejistá'),
(12,	'twine',	'Twine import',	'Unity, Twine, unity custom editors',	'2022',	'data-jineni/jineni2-twine-%d.jpg',	3,	'<p>\n             Snaha přesunout tvorbu větvících se dialogů do rychlejšího prototypování v <a href=\"https://twinery.org/\">Twinu</a>\n         </p>',	'https://gitlab.com/urg.boris/jineni-twine',	'',	'',	'Návrat ke starému kódu je překvapivě příjemný, Twine je trochu divný ale pomůže',	'Příliž mnoho času s tvorbou vlastního twine export formátu, který se stejně nepoužije'),
(13,	'youhouber',	'YouHouber',	'Unity, shaders, raymarching',	'2022',	'data-jineni/youhouber-%d.jpg',	1,	'<p>\r\n             Raymarchingoidní generátor hub\r\n         </p>',	'https://gitlab.com/urg.boris/jineni2-youhouber',	'',	'youhouber1.rar',	'Shadery jsou super, Raymarching mocný',	'Shadery jsou bolest'),
(14,	'lejem',	'Lejem! Simulator',	'Unity, 3d physic experimenty, Photogrammetry, Blender',	'2022',	'data-jineni/lejem-%d.jpg',	2,	'<p>\r\n            Zjistit jak vytvořit něco reálně vypadajícího a reálně fungujícího, Photogrammetry + Unity fisyka\r\n         </p>',	'https://gitlab.com/urg.boris/lejem-simulator',	'',	'lejem.rar',	'3D scany světa kolem do herního prostředí, blender přestává být strašidlo',	'Velmi moc pokus-omylů s dosažením herně-fyzikálních výsledků, které je potřeba přepracovat aby to mohl hrát i někdo, kdo to neprogramoval'),
(15,	'minascanabis',	'Minas Canabis',	'Macromedia Flash',	'2009',	'data-minas/minas%d.png',	5,	'<p>\r\n            Z historie neklidných počátků Smislsoftu\r\n        </p>',	'',	'',	'minas.rar',	'Flash je legrace, programování taky',	'Páteř se začína křivit'),
(16,	'minasdatadisk',	'Datadisk k Minas Canabis',	'Macromedia Flash',	'2010',	'data-minas/b-minas%d.jpg',	4,	'<p>\r\n            Nevydané pokračování dnes již legendární Minas Canabis! <br/>\r\n            Tentokrát hraješ za opačnou stranu než v prvním díle - za Pičiriny - sveřepý to národ ze šumavských hvozdů.<br/>\r\n            Že jsi o hře nikdy neslyšel? Měl bys vyměnit mechanismus toho jak si vzrůstal! <br/>            \r\n        </p>',	'',	'',	'bonusy.rar',	'Význam custom editor tools',	'Vůbec si na to období nepamatuju, tak bych čekal, že celý bylo strašný'),
(17,	'preprasor',	'Preprasor',	'PHP, LaTex, phpUnit, CSS doc',	'2016',	'',	0,	'<p>\nCSS preprocessor v PHP z dávných studentskýh let\n</p>',	'https://bitbucket.org/plesnif/diplomek/',	'',	'',	'Testování lowlevel věciček dokáže ohlalit jinak neodhalitelné',	'100% codecoverage is maddness'),
(18,	'marionetarium',	'Marionetárium',	'Nette,  AdminLte',	'2018',	'data-web/m-%d.png',	2,	'<p>\nKompletace a tvorba webového katalogu soukromé sbírky loutek\n</p>',	'https://gitlab.com/urg.boris/marionetarium-cropped',	'https://www.marionetarium.cz/',	'',	'Netrávit čas v CSS, organizace kolektace',	'je jich moc!'),
(19,	'smislsoft',	'Smislsoft web',	'PHP8, Nette3, nextrasORM,  phpStan, Codesniffer, Docker',	'2022',	'',	0,	'<p>\nDokumentace velkých i malých projektů a jejich mentálních procesů \n</p>',	'https://gitlab.com/urg.boris/smislsoft-web',	'https://smislsoft.online/',	'',	'Nastavit docker, nextras',	'Nastavit docker');

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `pass` varchar(250) NOT NULL,
  `role` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 2022-06-28 10:13:54
